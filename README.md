# Chrome Useragent for Firefox

Despite Firefox have WebRTC support, Steam insisted that voice chat has to be Chrome-exclusive. Here's a quick fix.

[Click me to install](https://gitlab.com/ninfy/steamcord/raw/master/uafix_steamcord.user.js)

# Vietnamese Localization for react-native Steam Chat

## Installation:


- Add the userscripts into your list - one for shared, one for friendsui.

[friendsui](https://gitlab.com/ninfy/steamcord/raw/master/friendsui_vietnamese.user.js)

[shared](https://gitlab.com/ninfy/steamcord/raw/master/shared_vietnamese.user.js)

- Optional: If you have strings conflicts, add these two lines into your ublock filter list.

```
https://steamcommunity-a.akamaihd.net/public/javascript/webui/localization/friendsui_english.js*
https://steamcommunity-a.akamaihd.net/public/javascript/webui/localization/shared_english.js*
```

## Bug(s):

- FriendGroup_Online, FriendGroup_Offline and FriendGroup_IncomingInvites aren't injected.

