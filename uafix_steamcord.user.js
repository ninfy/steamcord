// ==UserScript==
// @name         Useragent Fix for Firefox (Steam Chat/SteamTV)
// @namespace    UAFix.Steamcord
// @version      1.1
// @description  Change the User Agent on Firefox to Chrome 67
// @icon         https://steamcommunity.com/favicon.ico
// @author       ninf
// @match        https://steamcommunity.com/chat*
// @match        https://steam.tv/*
// @grant        none
// ==/UserScript==

navigator.__defineGetter__('userAgent', function(){

return 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.57 Safari/537.36';

});